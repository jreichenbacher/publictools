################################################################################
#
# By Jason Stock.
# This script will make a private/publics keypair, register it to your system,
# and output the public key to be coppied to the atlassian web client for ssh
# key authentication.
#
################################################################################

#!/bin/bash

outdir=~/.ssh_JReichenbacherGroup
fname=JReichenbacherGroup_AtlassianKey
#sshagent=$(eval `ssh-agent` | cut -d' ' -f3)

#does directory exist
if [ ! -d ${outdir} ]; then
  #make if it doesnt
  echo "Making ${outdir} for storing the ssh key"
  mkdir ${outdir}
  echo "Setting permissions for keyfiles."
  chmod 700 ${outdir}
else
  echo "Checking permissions of ${outdir}"
  if [ ! -O ${outdir} ]; then
    echo "Can't procede. You are not the owner of ${outdir}"
    exit 2
  else
    if [ ! $(stat -c "%a" "${outdir}") == "700" ]; then
      echo "Permissions of the existing ${outdir} are not correct.\n"
      echo "Please  make sure ${outdir} is 700."
      echo "This can be done by running:"
      echo "chmod 700 ${outdir}"
      exit 2
    else
      echo "Directory Found. Permissions correct."
    fi
  fi
fi

#does file exist.
if [ -f ${outdir}/${fname} ] || [ -f ${outdir}/${fname}.pub  ]; then
  echo "Key files already exist. Abandoning all hope."
  exit 2
fi

ssh-keygen -t rsa -N "" -f ${outdir}/${fname}
if [ -f ${outdir}/${fname} ] && [ -f ${outdir}/${fname}.pub ]; then
  echo "Files generated."
  chmod 500 ${outdir}/${fname}
  chmod 500 ${outdir}/${fname}.pub
else
  echo "Key files not found. Failed to generate."
  exit 2
fi

eval `ssh-agent`
ssh-add ${outdir}/${fname}

#edit user sshconfig.
#this section need error handeling
echo "1"
if ! grep -q bitbucket.org ~/.ssh/config ; then
  echo "2"
  echo "Now appending .ssh/config"
  if [ ! -f ~/.ssh/config ]; then
    touch ~/.ssh/config
  fi
  chmod 700 ~/.ssh/config
  printf "Host bitbucket.org\n  user git\n  IdentityFile ${outdir}/${fname}" \
    >> ~/.ssh/config
  chmod 500 ~/.ssh/config
fi


echo "So far so good. (I think.) Now to copy the public key to Atlassian."
printf "\n\nBEGIN PUBLIC KEY FILE CONTENTS\n\n"
cat ${outdir}/${fname}.pub
printf "\n\nEND PUBLIC KEY FILE CONTENTS\n\n"
